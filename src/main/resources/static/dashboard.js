/**
 * Created by Muhammad_dio on 11/02/2018.
 */

kodeBoking=[];
barang=[];
bulan=[];
dataUniv=[];
pickUniv=[];
pickDepart=[];

function showAll() {
    loadData();
    loadData2();
    loadData3();
    loadData4();
    loadData5();
}

function  loadData() {
    $.ajax({
        url:'/dashboard/getdata',
        type :'GET',
        success:function (data) {
            kodeBoking=data;
            console.log(kodeBoking);
            grafik();
            //nilai();
        },error:function () {
            alert('failed load data')
        }

    });
}


function  loadData2() {
    $.ajax({
        url:'/dashboard/getdata2',
        type :'GET',
        success:function (data) {
           bulan=data;
            console.log(bulan);
            grafik2();
        },error:function () {
            alert('failed load data')
        }
    });
}

function  loadData4() {
    $.ajax({
        url:'/dashboard/getdata4',
        type :'GET',
        success:function (data) {
            pickUniv=data;
            console.log(pickUniv);
            grafik4();
        },error:function () {
            alert('failed load data')
        }
    });
}

function  loadData5() {
    $.ajax({
        url:'/dashboard/getdata5',
        type :'GET',
        success:function (data) {
            pickDepart=data;
            console.log(pickDepart);
            grafik5();
        },error:function () {
            alert('failed load data')
        }
    });
}


function  loadData3() {
    $.ajax({
        url:'/dashboard/getdata3',
        type :'GET',
        success:function (data) {
            dataUniv=data;
            grafik3();
            //bulan=data;
            console.log(dataUniv);
            //grafik2();
        },error:function () {
            alert('failed load data')
        }
    });
}


function grafik() {
    var ctx = document.getElementById("tes");
    var mybarChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["PENGAJUAN", "PROSES","SELESAI"],
            datasets: [{
                label: 'Mahasiswa',
                backgroundColor: "#26B99A",
                data: [parseInt(kodeBoking[2]), parseInt(kodeBoking[4]),parseInt(kodeBoking[6])]
            }, {
                label: 'SMK',
                backgroundColor: "#03586A",
                data: [parseInt(kodeBoking[1]),parseInt(kodeBoking[3]),parseInt(kodeBoking[5])]
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

function grafik2() {
    var ctx = document.getElementById("grapik2");
    var mybarChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["PENGAJUAN", "PROSES","SELESAI"],
            datasets: [{
                label: 'Mahasiswa',
                backgroundColor: "#26B99A",
                data: [parseInt(bulan[2]), parseInt(bulan[4]),parseInt(bulan[6])]
            }, {
                label: 'SMK',
                backgroundColor: "#03586A",
                data: [parseInt(bulan[1]),parseInt(bulan[3]),parseInt(bulan[5])]
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}


function grafik4() {
    var ctx = document.getElementById("grapik4");
    var mybarChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["PENGAJUAN", "PROSES","SELESAI"],
            datasets: [{
                label: 'Mahasiswa',
                backgroundColor: "#26B99A",
                data: [parseInt(pickUniv[2]), parseInt(pickUniv[4]),parseInt(pickUniv[6])]
            }, {
                label: 'SMK',
                backgroundColor: "#03586A",
                data: [parseInt(pickUniv[1]),parseInt(pickUniv[3]),parseInt(pickUniv[5])]
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

function grafik3() {
    var ctx = document.getElementById("grapik3");
    var mybarChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [dataUniv[0], dataUniv[1],dataUniv[2],dataUniv[3],dataUniv[4]],
            datasets: [{
                label: 'Mahasiswa',
                backgroundColor: "#26B99A",
                data: [parseInt(dataUniv[5]), parseInt(dataUniv[6]),parseInt(dataUniv[7]),parseInt(dataUniv[8]),parseInt(dataUniv[9])]
            }, {
                label: 'SMK',
                backgroundColor: "#03586A",
                data: ["dio","dio","dio","dio","dio"]
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}


function grafik5() {
    var ctx = document.getElementById("grapik5");
    var mybarChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["PENGAJUAN", "PROSES","SELESAI"],
            datasets: [{
                label: 'Mahasiswa',
                backgroundColor: "#26B99A",
                data: [parseInt(pickDepart[2]), parseInt(pickDepart[4]),parseInt(pickDepart[6])]
            }, {
                label: 'SMK',
                backgroundColor: "#03586A",
                data: [parseInt(pickDepart[1]),parseInt(pickDepart[3]),parseInt(pickDepart[5])]
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}
