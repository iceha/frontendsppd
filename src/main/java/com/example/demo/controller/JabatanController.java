package com.example.demo.controller;

import com.example.demo.dao.JabatanDao;
import com.example.demo.entity.Jabatan;
import com.example.demo.entity.Pegawai;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/Jabatan")
public class JabatanController {

    @Autowired
    private JabatanDao jd;

    List<Jabatan> jabatanList = new ArrayList<>();
    Jabatan jabatan=new Jabatan();


    @RequestMapping("setupJabatan")
    public void showPanitia(Model m)
    {
        jabatan=new Jabatan();
        jabatanList.clear();
        jabatanList=jd.getALL();
        int no=1;

        m.addAttribute("jabatanList",jabatanList);
    }

    @RequestMapping("detailJabatan")
    public void detailPanitia(Model m)
    {
        if (jabatan==null)
        {
            Jabatan jabatan=new Jabatan();
            this.jabatan=jabatan;
        }
        else
        {
            jabatan.setJabatan(jabatan.getNamaJabatan());
        }

        m.addAttribute("jAbatan",jabatan);
    }

    @RequestMapping(value = "/save",method = RequestMethod.POST)
    @Transactional
    public String save(Jabatan jabatan,RedirectAttributes redirectAttributes)
    {
        if (cekfieldKosong(jabatan)==true)
        {
            redirectAttributes.addFlashAttribute("message", "Gagal Simpan, Terdapat Field Yang masih kosong");
            redirectAttributes.addFlashAttribute("alertClass", "alert-danger");
            this.jabatan=jabatan;
            return "redirect:detailJabatan";
        }
        else if (cekKodesama(jabatan)==true  )
        {
            redirectAttributes.addFlashAttribute("message", "Gagal Simpan, Data Telah Digunakan");
            redirectAttributes.addFlashAttribute("alertClass", "alert-danger");
            this.jabatan=jabatan;
            return "redirect:detailJabatan";
        }
        else if (cekKarakterHuruf(jabatan)==false)
        {
            redirectAttributes.addFlashAttribute("message", "Gagal Simpan, Format Salah");
            redirectAttributes.addFlashAttribute("alertClass", "alert-danger");
            this.jabatan=jabatan;
            return "redirect:detailJabatan";
        }
        else
        {
            redirectAttributes.addFlashAttribute("message", "Berhasil Di Simpan");
            redirectAttributes.addFlashAttribute("alertClass", "alert-success");
        }
        if (this.jabatan.getIdJabatan() !=null)
        {
            jabatan.setIdJabatan(this.jabatan.getIdJabatan());
        }
        jabatan.setJabatan(jabatan.getNamaJabatan());
        jd.save(jabatan);

        return "redirect:setupJabatan";
    }



    public Boolean cekfieldKosong(Jabatan jabatan)
    {
        Boolean result=false;
        if (jabatan.getNamaJabatan().equalsIgnoreCase(""))
        {
            result=true;
            return result;
        }
        return result;
    }

    public Boolean cekKodesama(Jabatan jabatan) {
        Boolean result = false;
        List<Jabatan> list = new ArrayList<>();
        list = jd.getALL();
        for (Jabatan jabatan1:list) {
            if (jabatan.getNamaJabatan().equalsIgnoreCase("")) {
                result = true;
                return result;
            }
        }
        return result;
    }

    public Boolean cekKarakterHuruf(Jabatan jabatan)
    {
        Boolean result=false;
        String kata=jabatan.getNamaJabatan();
        try {
            Integer cek=Integer.parseInt(kata);
        }catch (Exception e)
        {
            result=true;
            return result;
        }

        return result;
    }


    @RequestMapping(value = "hapusJabatan",method = RequestMethod.GET)
    public String hapusJabatan(@RequestParam(value = "id",required = false) int id)
    {
        jabatan=jd.getJabatanByID(id);
        jd.delete(jabatan);
        jabatan=new Jabatan();
        return "redirect:setupJabatan";
    }

    @RequestMapping(value = "/editJabatan",method = RequestMethod.GET)
    public String showEditJabatan(@RequestParam(value = "id",required = false) int id)
    {
        jabatan=new Jabatan();
        jabatan=jd.getJabatanByID(id);
        return "redirect:detailJabatan";
    }

    @RequestMapping(value = "findJabatan",method = RequestMethod.GET)
    @ResponseBody
    public  Jabatan findJabatan(@RequestParam(value = "id",required = false) int id)
    {
        Jabatan result=new Jabatan();
        result=jd.getJabatanByID(id);
        return result;
    }

    @RequestMapping(value = "findAllJabatan",method = RequestMethod.GET)
    @ResponseBody
    public  List<Jabatan> findAllJabatan()
    {
        List<Jabatan> result=new ArrayList<>();
        result=jd.getALL();
        return result;
    }
}
