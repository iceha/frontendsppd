package com.example.demo.controller;

import com.example.demo.dao.JabatanDao;
import com.example.demo.dao.PegawaiDao;
import com.example.demo.dao.SppdDao;
import com.example.demo.dao.SuratDao;
import com.example.demo.entity.Sppd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/Dashboard")
public class DashboardController {
    @Autowired
    private SuratDao sd;
    @Autowired
    private PegawaiDao pd;
    @Autowired
    private JabatanDao jd;
    @Autowired
    private SppdDao sppdDao;

    @RequestMapping("dashboard")
    public void showPanitia(Model m)
    {
        List<Sppd> sppdList = new ArrayList<>();
        sppdList.clear();
        sppdList=sppdDao.getALL();
        int suratTotal=0;
        int pegawaiTotal=0;
        int jabatanTotal=0;

        suratTotal=sd.getALL().size();
        pegawaiTotal=pd.getALL().size();
        jabatanTotal=jd.getALL().size();

        int sppdBerjalan=0;
        int sppdSelesai=0;
        int sppdSelanjutnya=0;

        Calendar calendar=Calendar.getInstance();

        Date tglSkrg=calendar.getTime();

        for (Sppd sppd:sppdList)
        {
            if (sppd.getTanggalKembali().before(tglSkrg))
            {
                sppd.setStatus("Selesai");
                sppdSelesai++;
            }
            if (sppd.getTanggalBerangkat().after(tglSkrg))
            {
                sppd.setStatus("Belum Berjalan");
                sppdSelanjutnya++;
            }
            if (sppd.getTanggalBerangkat().before(tglSkrg) && sppd.getTanggalKembali().after(tglSkrg))
            {
                sppd.setStatus("Berjalan");
                sppdBerjalan++;
            }
        }


        m.addAttribute("sppdSelanjutnya",sppdSelanjutnya);
        m.addAttribute("sppdSelesai",sppdSelesai);
        m.addAttribute("sppdBerjalan",sppdBerjalan);
        m.addAttribute("sppdList",sppdList);
        m.addAttribute("suratTotal",suratTotal);
        m.addAttribute("pegawaiTotal",pegawaiTotal);
        m.addAttribute("jabatanTotal",jabatanTotal);
    }
}
