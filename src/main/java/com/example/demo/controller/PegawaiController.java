package com.example.demo.controller;

import com.example.demo.dao.JabatanDao;
import com.example.demo.dao.PegawaiDao;
import com.example.demo.entity.Jabatan;
import com.example.demo.entity.Pegawai;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/Pegawai")
public class PegawaiController {

    @Autowired
    private JabatanDao jd;

    @Autowired
    private PegawaiDao pd;

    List<Pegawai> pegawaiList = new ArrayList<>();
    Pegawai pegawai=new Pegawai();


    @RequestMapping("setupPegawai")
    public void showPanitia(Model m)
    {
        pegawai=new Pegawai();
        pegawaiList.clear();
        pegawaiList=pd.getALL();

        m.addAttribute("pegawaiList",pegawaiList);
    }

    @RequestMapping("detailPegawai")
    public void detailPanitia(Model m)
    {
        if (pegawai==null)
        {
            Pegawai pegawai=new Pegawai();
            this.pegawai=pegawai;
        }

        List<String> listJabatan=new ArrayList<>();

        List<Jabatan> list =new ArrayList<>();
        list=jd.getALL();
        for (Jabatan jabatan:list)
        {
            listJabatan.add(Integer.toString(jabatan.getIdJabatan()));
        }


        m.addAttribute("listJabatan",jd.getALL());
        m.addAttribute("pegawai",pegawai);
    }

    @RequestMapping(value = "/save",method = RequestMethod.POST)
    @Transactional
    public String save(Pegawai pegawai,RedirectAttributes redirectAttributes)
    {
        if (cekfieldKosong(pegawai)==true)
        {
            redirectAttributes.addFlashAttribute("message", "Gagal Simpan, Terdapat Field Yang masih kosong");
            redirectAttributes.addFlashAttribute("alertClass", "alert-danger");
            this.pegawai=pegawai;
            return "redirect:detailPegawai";
        }
        else if (cekFormatNip(pegawai)==true)
        {
            redirectAttributes.addFlashAttribute("message", "Gagal Simpan, Format Nip Salah");
            redirectAttributes.addFlashAttribute("alertClass", "alert-danger");
            this.pegawai=pegawai;
            return "redirect:detailPegawai";
        }
        else if (cekKodesama(pegawai)==true  )
        {
            redirectAttributes.addFlashAttribute("message", "Gagal Simpan, Data Telah Digunakan");
            redirectAttributes.addFlashAttribute("alertClass", "alert-danger");
            this.pegawai=pegawai;
            return "redirect:detailPegawai";
        }
        else if (cekKarakterHuruf(pegawai)==false)
        {
            redirectAttributes.addFlashAttribute("message", "Gagal Simpan, Format Salah");
            redirectAttributes.addFlashAttribute("alertClass", "alert-danger");
            this.pegawai=pegawai;
            return "redirect:detailPegawai";
        }
        else
        {
            redirectAttributes.addFlashAttribute("message", "Berhasil Di Simpan");
            redirectAttributes.addFlashAttribute("alertClass", "alert-success");
        }

        Jabatan jabatan=new Jabatan();
        jabatan=jd.getJabatanByID(pegawai.getJabatan().getIdJabatan());
        pegawai.setJabatan(jabatan);
        pd.save(pegawai);
        return "redirect:setupPegawai";
    }

    public Boolean cekFormatNip(Pegawai pegawai)
    {
        Boolean result=false;
        String cek=pegawai.getNip().toString();
        if (cek.length() <10)
        {
            result=true;
            return result;
        }
        return result;
    }


    public Boolean cekfieldKosong(Pegawai pegawai)
    {
        Boolean result=false;
        if (pegawai.getNama().equalsIgnoreCase(""))
        {
            result=true;
            return result;
        }
        return result;
    }

    public Boolean cekKodesama(Pegawai pegawai) {
        Boolean result = false;
        List<Pegawai> list = new ArrayList<>();
        list = pd.getALL();
        for (Pegawai pegawai1 : list) {
            if (pegawai.getNip() == pegawai1.getNip()) {
                result = true;
                return result;
            }
        }
        return result;
    }

    public Boolean cekKarakterHuruf(Pegawai pegawai)
    {
        Boolean result=false;
        String kata=pegawai.getNama();

        try {
            Integer cek=Integer.parseInt(kata);
        }catch (Exception e)
        {
            result=true;
            return result;
        }

        return result;
    }


    @RequestMapping(value = "hapusPegawai",method = RequestMethod.GET)
    public String hapusPegawai(@RequestParam(value = "id",required = false) int id)
    {
        pegawai=pd.getPegawaiByID(id);
        pd.delete(pegawai);
        pegawai=new Pegawai();
        return "redirect:setupPegawai";
    }

    @RequestMapping(value = "/editPegawai",method = RequestMethod.GET)
    public String showEditPegawai(@RequestParam(value = "id",required = false) int id)
    {
        pegawai=new Pegawai();
        pegawai=pd.getPegawaiByID(id);
        return "redirect:detailPegawai";
    }


}
