package com.example.demo.controller;

import com.example.demo.dao.JabatanDao;
import com.example.demo.dao.SppdDao;
import com.example.demo.dao.SuratDao;
import com.example.demo.dao.UserDao;
import com.example.demo.entity.Pegawai;
import com.example.demo.entity.Sppd;
import com.example.demo.entity.Surat;
import com.example.demo.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("Sppd")
public class SppdController {
    @Autowired
    private SppdDao sd;

    @Autowired
    private UserDao ud;
    @Autowired
    private JabatanDao jd;

    List<Sppd> sppdList = new ArrayList<>();
    Sppd sppd=new Sppd();

    Double lotitude=-6.1669604;
    Double longtitude=106.7269653;

    @RequestMapping(value = "/getdata",method = RequestMethod.GET)
    @ResponseBody
    public Double[] getData() {
        Double map[] = new Double[3];
        map[0]=lotitude;
        map[1]=longtitude;
        return map;
    }

    @RequestMapping("/mapSppd")
    public void showMap()
    {

    }


    @RequestMapping("setupSppd")
    public void showPanitia(Model m)
    {
        sppd=new Sppd();
        sppdList.clear();
        sppdList=sd.getALL();
        int no=1;

        for (Sppd sppd:sppdList)
        {
            String alamatpertama="";
            String alamat="https://www.google.com/maps/?q=";
            alamatpertama=alamat+sppd.getLatitudePertama()+","+sppd.getLongitudePertama();
            sppd.setLokasiMapAwal(alamatpertama);

            String alamatakhir= "";
            alamatakhir = alamat+sppd.getLatitudeAkhir()+","+sppd.getLongitudeAkhir();
            sppd.setLokasiMapAkhir(alamatakhir);
        }

        //m.addAttribute("alamat",alamat);
        m.addAttribute("sppdList",sppdList);
    }

    @RequestMapping("detailSppd")
    public void detailPanitia(Model m)
    {
        if (sppd==null)
        {
            sppd=new Sppd();
        }

        List<String> listTranspotasi=new ArrayList<>();
        listTranspotasi.add("Pesawat");
        listTranspotasi.add("Kereta");
        listTranspotasi.add("Mobil");

        List<User> listUser=new ArrayList<>();
        listUser=ud.getALL();
        m.addAttribute("listJabatan",jd.getALL());
        m.addAttribute("listUser",listUser);
        m.addAttribute("listTranspotasi",listTranspotasi);
        m.addAttribute("sppd",sppd);
    }

    @RequestMapping(value = "/simpan",method = RequestMethod.POST)
    @Transactional
    public String simpanSppd(Sppd sppd,RedirectAttributes redirectAttributes)
    {
        return "redirect:detailSppd";
    }

    @RequestMapping(value = "/save",method = RequestMethod.POST)
    @Transactional
    public String save(Sppd sppd,RedirectAttributes redirectAttributes)
    {
        if (cekfieldKosong(sppd)==true)
        {
            redirectAttributes.addFlashAttribute("message", "Gagal Simpan, Terdapat Field Yang masih kosong");
            redirectAttributes.addFlashAttribute("alertClass", "alert-danger");
            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
            dt.format(sppd.getTanggalBerangkat());

        //    this.sppd=sppd;
            return "redirect:detailSppd";
        }
        else if (cekFormatNip(sppd)==true)
        {
            redirectAttributes.addFlashAttribute("message", "Gagal Simpan, Terdapat Field Yang masih kosong");
            redirectAttributes.addFlashAttribute("alertClass", "alert-danger");
          //  this.sppd=sppd;
            return "redirect:detailSppd";
        }
        else
        {
            redirectAttributes.addFlashAttribute("message", "Berhasil Di Simpan");
            redirectAttributes.addFlashAttribute("alertClass", "alert-success");
        }

        User user=new User();
        user=ud.getUserByID(sppd.getIdUser());


        String tglAwal=konvertDate(sppd.getTanggalBerangkat());
        String tglAkhir=konvertDate(sppd.getTanggalKembali());
List<Date> list=new ArrayList<>();
list=dateConvert(tglAwal,sppd.getStatusMulai()+":00",tglAkhir,sppd.getStatusSelesai()+":00");

if (list.size()>0)
{
    sppd.setTanggalBerangkat(list.get(0));
    sppd.setTanggalKembali(list.get(1));
}
sppd.setStatusSelesai(null);
sppd.setStatusMulai(null);


        sppd.setAlamatAkhir("");
        sppd.setAlamatPertama("");
        sppd.setImageAwal("");
        sppd.setImageAkhir("");
        sppd.setBeritaAcara("");
        sppd.setNama(user.getUsername());
        sppd.setToken(user.getToken());
        sppd.setStatusMulai("0");
        sppd.setStatusSelesai("0");
        sd.save(sppd);
        return "redirect:setupSppd";
    }


    public Boolean cekFormatNip(Sppd sppd)
    {
        Boolean result=false;
        String cek=sppd.getNip();
        if (cek.length() <10)
        {
            result=true;
            return result;
        }
        return result;
    }


    public Boolean cekfieldKosong(Sppd sppd)
    {
        Boolean result=false;
        if (sppd.getNoSurat().equalsIgnoreCase("") ||
                sppd.getKegiatan().equalsIgnoreCase("")
                || sppd.getLokasiBerangkat().equalsIgnoreCase("") || sppd.getLokasiTujuan().equalsIgnoreCase(""))
        {
            result=true;
            return result;
        }
        return result;
    }







    public static String konvertDate(Date tgl)
    {
        String result="";
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
        try {
            result = dt.format(tgl);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static List<Date> dateConvert (String tgl1,String jam1,String tgl2,String jam2)
    {
        List<Date> result=new ArrayList<>();
        String berangkat = tgl1+ " " + jam1;
        String kembali = tgl2+" "+jam2;
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date tanggalBerangkat = null, tanggalSelesai = null;

        try {
            tanggalBerangkat = dt.parse(berangkat);
            tanggalSelesai = dt.parse(kembali);
            result.add(tanggalBerangkat);
            result.add(tanggalSelesai);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(value = "hapusSppd",method = RequestMethod.GET)
    public String hapusJabatan(@RequestParam(value = "id",required = false) int id)
    {
        sppd=sd.getSppdByID(id);
        sd.delete(sppd);
        sppd=new Sppd();
        return "redirect:setupSppd";
    }

    @RequestMapping(value = "/editSppd",method = RequestMethod.GET)
    public String showEditJabatan(@RequestParam(value = "id",required = false) int id)
    {
        sppd=new Sppd();
        sppd=sd.getSppdByID(id);
        return "redirect:detailSppd";
    }

    @RequestMapping(value = "/showLokasi",method = RequestMethod.GET)
    public String showLokasi(@RequestParam(value = "id",required = false) int id)
    {
        sppd=new Sppd();
        sppd=sd.getSppdByID(id);
        lotitude=sppd.getLatitudePertama();
        longtitude=sppd.getLongitudePertama();
        return "redirect:mapSppd";
    }
}
