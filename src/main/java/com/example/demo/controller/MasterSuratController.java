package com.example.demo.controller;

import com.example.demo.dao.JabatanDao;
import com.example.demo.dao.SuratDao;
import com.example.demo.entity.Jabatan;
import com.example.demo.entity.Surat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/MasterSurat")
public class MasterSuratController {

    @Autowired
    private SuratDao sd;

    List<Surat> suratList = new ArrayList<>();
    Surat surat=new Surat();

    @RequestMapping("setupMasterSurat")
    public void showPanitia(Model m)
    {
        surat=new Surat();
        suratList.clear();
        suratList=sd.getALL();
        int no=1;
        for (Surat surat:suratList)
        {
            surat.setNo(no);
            no++;
        }
        m.addAttribute("suratList",suratList);
    }

    @RequestMapping("detailMasterSurat")
    public void detailPanitia(Model m)
    {
        if (surat==null)
        {
            Surat surat=new Surat();
            this.surat=surat;
        }
        m.addAttribute("surat",surat);
    }

    @RequestMapping(value = "/save",method = RequestMethod.POST)
    @Transactional
    public String save(Surat surat)
    {
        sd.save(surat);
        return "redirect:setupMasterSurat";
    }

    @RequestMapping(value = "hapusSurat",method = RequestMethod.GET)
    public String hapusJabatan(@RequestParam(value = "id",required = false) int id)
    {
        surat=sd.getSuratByID(id);
        sd.delete(surat);
        surat=new Surat();
        return "redirect:setupMasterSurat";
    }

    @RequestMapping(value = "/editSurat",method = RequestMethod.GET)
    public String showEditJabatan(@RequestParam(value = "id",required = false) int id)
    {
        surat=new Surat();
        surat=sd.getSuratByID(id);
        return "redirect:detailMasterSurat";
    }
}
