package com.example.demo.dao;

import com.example.demo.entity.Jabatan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface JabatanDao extends PagingAndSortingRepository<Jabatan,Integer> {
    @Query("FROM Jabatan order by id")
    public List<Jabatan> getALL();

    @Query("FROM Jabatan where id=:id")
    public List<Jabatan> getListidJenisAcaraByID(@Param("id") int id);

    @Query("FROM Jabatan where id =:id")
    public Jabatan getJabatanByID(@Param(("id"))int id);
}
