package com.example.demo.dao;

import com.example.demo.entity.Jabatan;
import com.example.demo.entity.Pegawai;
import com.example.demo.entity.Sppd;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Khairunnisa on 06/06/2018.
 */
public interface SppdDao  extends PagingAndSortingRepository<Sppd,Integer> {
    @Query("FROM Sppd order by id desc")
    public List<Sppd> getALL();

    @Query("FROM Sppd where id=:id")
    public List<Sppd> getListidSppdByID(@Param("id") int id);

    @Query("FROM Sppd where id =:id")
    public Sppd getSppdByID(@Param(("id"))int id);
}
