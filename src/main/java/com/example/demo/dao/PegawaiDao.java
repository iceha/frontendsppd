package com.example.demo.dao;

import com.example.demo.entity.Jabatan;
import com.example.demo.entity.Pegawai;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PegawaiDao extends PagingAndSortingRepository<Pegawai,Integer> {
    @Query("FROM Pegawai order by id desc")
    public List<Pegawai> getALL();

    @Query("FROM Pegawai where id=:id")
    public List<Pegawai> getListidPegawaiByID(@Param("id") int id);

    @Query("FROM Pegawai where id =:id")
    public Pegawai getPegawaiByID(@Param(("id"))int id);
}
