package com.example.demo.dao;

import com.example.demo.entity.Surat;
import com.example.demo.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserDao extends PagingAndSortingRepository<User,Integer> {
    @Query("FROM User order by id")
    public List<User> getALL();

    @Query("FROM User where id=:id")
    public List<User> getListidUserByID(@Param("id") int id);

    @Query("FROM User where id =:id")
    public User getUserByID(@Param(("id"))int id);
}
