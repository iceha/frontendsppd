package com.example.demo.dao;

import com.example.demo.entity.Sppd;
import com.example.demo.entity.Surat;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Khairunnisa on 07/07/18.
 */
public interface SuratDao extends PagingAndSortingRepository<Surat,Integer> {
    @Query("FROM Surat order by id")
    public List<Surat> getALL();

    @Query("FROM Surat where id=:id")
    public List<Surat> getListidSuratByID(@Param("id") int id);

    @Query("FROM Surat where id =:id")
    public Surat getSuratByID(@Param(("id"))int id);

}
