package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name = "table_notification")
public class Notification implements Serializable {

    private static final long serialVersionUID = 8744093837026444842L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_notificaiton", nullable = false)
    @JsonProperty("id_notificaiton")
    private Integer idNotificaiton;

    @Column(name = "id_sppd", nullable = false)
    @JsonProperty("id_sppd")
    private Integer idSppd;

    @Column(name = "nama_sppd")
    @JsonProperty("nama_sppd")
    private String namaSppd;

    @Column(name = "status_notificaiton")
    @JsonProperty("status_notificaiton")
    private String statusNotificaiton;

    @Column(name = "tanggal_notification")
    @JsonProperty("tanggal_notification")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private Date tanggalNotification;


    //setter getter

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getIdNotificaiton() {
        return idNotificaiton;
    }

    public void setIdNotificaiton(Integer idNotificaiton) {
        this.idNotificaiton = idNotificaiton;
    }

    public Integer getIdSppd() {
        return idSppd;
    }

    public void setIdSppd(Integer idSppd) {
        this.idSppd = idSppd;
    }

    public String getNamaSppd() {
        return namaSppd;
    }

    public void setNamaSppd(String namaSppd) {
        this.namaSppd = namaSppd;
    }

    public String getStatusNotificaiton() {
        return statusNotificaiton;
    }

    public void setStatusNotificaiton(String statusNotificaiton) {
        this.statusNotificaiton = statusNotificaiton;
    }

    public Date getTanggalNotification() {
        return tanggalNotification;
    }

    public void setTanggalNotification(Date tanggalNotification) {
        this.tanggalNotification = tanggalNotification;
    }
}
