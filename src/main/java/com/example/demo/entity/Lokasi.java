package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "table_lokasi")
public class Lokasi implements Serializable {

    private static final long serialVersionUID = -6556453392795740804L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_lokasi")
    @JsonProperty("id_lokasi")
    private Integer idLokasi;

    @Column(name = "nama_lokasi", nullable = false)
    @JsonProperty("nama_lokasi")
    private String namaLokasi;

    //setter getter

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getIdLokasi() {
        return idLokasi;
    }

    public void setIdLokasi(Integer idLokasi) {
        this.idLokasi = idLokasi;
    }

    public String getNamaLokasi() {
        return namaLokasi;
    }

    public void setNamaLokasi(String namaLokasi) {
        this.namaLokasi = namaLokasi;
    }
}
