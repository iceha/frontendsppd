package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name = "table_sppd")
public class Sppd implements Serializable {

    private static final long serialVersionUID = 6380191024483360263L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_sppd", nullable = false)
    @JsonProperty("id_sppd")
    private Integer idSppd;

    @Column(name = "no_surat")
    @JsonProperty("no_surat")
    private String noSurat;

    @Column(nullable = false)
    private String nama;

    @Column(nullable = false)
    private String nip;

    @Column(name = "id_user", nullable = false)
    @JsonProperty("id_user")
    private Integer idUser;

    @Column(name = "id_jabatan", nullable = false)
    @JsonProperty("id_jabatan")
    private String idJabatan;

    @Column(nullable = false)
    private String kegiatan;

    @Column(name = "jenis_transpotasi")
    @JsonProperty("jenis_transpotasi")
    private String jenisTranspotasi;

    @Column(name = "lokasi_berangkat")
    @JsonProperty("lokasi_berangkat")
    private String lokasiBerangkat;

    @Column(name = "lokasi_tujuan")
    @JsonProperty("lokasi_tujuan")
    private String lokasiTujuan;

    @Column(name = "tanggal_berangkat")
    @JsonProperty("tanggal_berangkat")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private Date tanggalBerangkat;

    @Column(name = "tanggal_kembali")
    @JsonProperty("tanggal_kembali")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private Date tanggalKembali;

    @Column(name = "tanggal_share_berangkat")
    @JsonProperty("tanggal_share_berangkat")
    private Date tanggalShareBerangkat;

    @Column(name = "tanggal_share_kembali")
    @JsonProperty("tanggal_share_kembali")
    private Date tanggalShareKembali;

    @Column(name = "latitude_pertama")
    @JsonProperty("latitude_pertama")
    private double latitudePertama;

    @Column(name = "alamat_pertama")
    @JsonProperty("alamat_pertama")
    private String alamatPertama;

    @Column(name = "longitude_pertama")
    @JsonProperty("longitude_pertama")
    private double longitudePertama;

    @Column(name = "latitude_akhir")
    @JsonProperty("latitude_akhir")
    private double latitudeAkhir;

    @Column(name = "longitude_akhir")
    @JsonProperty("longitude_akhir")
    private double longitudeAkhir;

    @Column(name = "alamat_akhir")
    @JsonProperty("alamat_akhir")
    private String alamatAkhir;

    @Column(name = "status_mulai")
    @JsonProperty("status_mulai")
    private String statusMulai;

    @Column(name = "status_selesai")
    @JsonProperty("status_selesai")
    private String statusSelesai;

    @Column(name = "token")
    @JsonProperty("token")
    private String token;

    private String status;

    private String lokasiMapAwal;

    private String lokasiMapAkhir;


    @Column(name = "image_awal")
    @JsonProperty("image_awal")
    private String imageAwal;

    @Column(name = "image_akhir")
    @JsonProperty("image_akhir")
    private String imageAkhir;

    @Column(name = "berita_acara")
    @JsonProperty("berita_acara")
    private String beritaAcara;

    @Override
    public String toString() {
        return "Sppd{" +
                "idSppd=" + idSppd +
                ", noSurat='" + noSurat + '\'' +
                ", nama='" + nama + '\'' +
                ", nip='" + nip + '\'' +
                ", idUser=" + idUser +
                ", idJabatan='" + idJabatan + '\'' +
                ", kegiatan='" + kegiatan + '\'' +
                ", jenisTranspotasi='" + jenisTranspotasi + '\'' +
                ", lokasiBerangkat='" + lokasiBerangkat + '\'' +
                ", lokasiTujuan='" + lokasiTujuan + '\'' +
                ", tanggalBerangkat=" + tanggalBerangkat +
                ", tanggalKembali=" + tanggalKembali +
                ", tanggalShareBerangkat=" + tanggalShareBerangkat +
                ", tanggalShareKembali=" + tanggalShareKembali +
                ", latitudePertama=" + latitudePertama +
                ", alamatPertama='" + alamatPertama + '\'' +
                ", longitudePertama=" + longitudePertama +
                ", latitudeAkhir=" + latitudeAkhir +
                ", longitudeAkhir=" + longitudeAkhir +
                ", alamatAkhir='" + alamatAkhir + '\'' +
                ", statusMulai='" + statusMulai + '\'' +
                ", statusSelesai='" + statusSelesai + '\'' +
                ", token='" + token + '\'' +
                ", status='" + status + '\'' +
                ", lokasiMapAwal='" + lokasiMapAwal + '\'' +
                ", lokasiMapAkhir='" + lokasiMapAkhir + '\'' +
                ", imageAwal='" + imageAwal + '\'' +
                ", imageAkhir='" + imageAkhir + '\'' +
                ", beritaAcara='" + beritaAcara + '\'' +
                '}';
    }


    //setter getter

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getIdSppd() {
        return idSppd;
    }

    public void setIdSppd(Integer idSppd) {
        this.idSppd = idSppd;
    }

    public String getNoSurat() {
        return noSurat;
    }

    public void setNoSurat(String noSurat) {
        this.noSurat = noSurat;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getIdJabatan() {
        return idJabatan;
    }

    public void setIdJabatan(String idJabatan) {
        this.idJabatan = idJabatan;
    }

    public String getKegiatan() {
        return kegiatan;
    }

    public void setKegiatan(String kegiatan) {
        this.kegiatan = kegiatan;
    }

    public String getJenisTranspotasi() {
        return jenisTranspotasi;
    }

    public void setJenisTranspotasi(String jenisTranspotasi) {
        this.jenisTranspotasi = jenisTranspotasi;
    }

    public String getLokasiBerangkat() {
        return lokasiBerangkat;
    }

    public void setLokasiBerangkat(String lokasiBerangkat) {
        this.lokasiBerangkat = lokasiBerangkat;
    }

    public String getLokasiTujuan() {
        return lokasiTujuan;
    }

    public void setLokasiTujuan(String lokasiTujuan) {
        this.lokasiTujuan = lokasiTujuan;
    }

    public Date getTanggalBerangkat() {
        return tanggalBerangkat;
    }

    public void setTanggalBerangkat(Date tanggalBerangkat) {
        this.tanggalBerangkat = tanggalBerangkat;
    }

    public Date getTanggalKembali() {
        return tanggalKembali;
    }

    public void setTanggalKembali(Date tanggalKembali) {
        this.tanggalKembali = tanggalKembali;
    }

    public double getLatitudePertama() {
        return latitudePertama;
    }

    public void setLatitudePertama(double latitudePertama) {
        this.latitudePertama = latitudePertama;
    }

    public String getAlamatPertama() {
        return alamatPertama;
    }

    public void setAlamatPertama(String alamatPertama) {
        this.alamatPertama = alamatPertama;
    }

    public double getLongitudePertama() {
        return longitudePertama;
    }

    public void setLongitudePertama(double longitudePertama) {
        this.longitudePertama = longitudePertama;
    }

    public double getLatitudeAkhir() {
        return latitudeAkhir;
    }

    public void setLatitudeAkhir(double latitudeAkhir) {
        this.latitudeAkhir = latitudeAkhir;
    }

    public double getLongitudeAkhir() {
        return longitudeAkhir;
    }

    public void setLongitudeAkhir(double longitudeAkhir) {
        this.longitudeAkhir = longitudeAkhir;
    }

    public String getAlamatAkhir() {
        return alamatAkhir;
    }

    public void setAlamatAkhir(String alamatAkhir) {
        this.alamatAkhir = alamatAkhir;
    }

    public String getStatusMulai() {
        return statusMulai;
    }

    public void setStatusMulai(String statusMulai) {
        this.statusMulai = statusMulai;
    }

    public String getStatusSelesai() {
        return statusSelesai;
    }

    public void setStatusSelesai(String statusSelesai) {
        this.statusSelesai = statusSelesai;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getTanggalShareBerangkat() {
        return tanggalShareBerangkat;
    }

    public void setTanggalShareBerangkat(Date tanggalShareBerangkat) {
        this.tanggalShareBerangkat = tanggalShareBerangkat;
    }

    public Date getTanggalShareKembali() {
        return tanggalShareKembali;
    }

    public void setTanggalShareKembali(Date tanggalShareKembali) {
        this.tanggalShareKembali = tanggalShareKembali;
    }

    public String getLokasiMapAwal() {
        return lokasiMapAwal;
    }

    public void setLokasiMapAwal(String lokasiMapAwal) {
        this.lokasiMapAwal = lokasiMapAwal;
    }

    public String getLokasiMapAkhir() {
        return lokasiMapAkhir;
    }

    public void setLokasiMapAkhir(String lokasiMapAkhir) {
        this.lokasiMapAkhir = lokasiMapAkhir;
    }

    public String getImageAwal() {
        return imageAwal;
    }

    public void setImageAwal(String imageAwal) {
        this.imageAwal = imageAwal;
    }

    public String getImageAkhir() {
        return imageAkhir;
    }

    public void setImageAkhir(String imageAkhir) {
        this.imageAkhir = imageAkhir;
    }

    public String getBeritaAcara() {
        return beritaAcara;
    }

    public void setBeritaAcara(String beritaAcara) {
        this.beritaAcara = beritaAcara;
    }
}