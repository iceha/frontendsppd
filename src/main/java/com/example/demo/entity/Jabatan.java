package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "table_jabatan")
public class Jabatan {
    private static final long serialVersionUID = 8744093837026444842L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_jabatan", nullable = false)
    private Integer idJabatan;


    @Column(nullable = false)
    private String jabatan;


    private String namaJabatan;

    //setter getter
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getIdJabatan() {
        return idJabatan;
    }

    public void setIdJabatan(Integer idJabatan) {
        this.idJabatan = idJabatan;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getNamaJabatan() {
        return namaJabatan;
    }

    public void setNamaJabatan(String namaJabatan) {
        this.namaJabatan = namaJabatan;
    }
}
