package com.example.demo.entity;

import javax.persistence.*;

@Entity
@Table(name = "s_users")
public class s_users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String username;
    private String password;
    private int no;
    private Boolean active;
    private String role;
    private String status;

    @ManyToOne
    @JoinColumn(name = "s_users_role")
    private com.example.demo.entity.s_users_role s_users_role;
//setter

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public com.example.demo.entity.s_users_role getS_users_role() {
        return s_users_role;
    }

    public void setS_users_role(com.example.demo.entity.s_users_role s_users_role) {
        this.s_users_role = s_users_role;
    }
}
