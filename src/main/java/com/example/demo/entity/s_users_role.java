package com.example.demo.entity;

import javax.persistence.*;

@Entity
@Table(name = "s_users_role")
public class s_users_role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer id_user;
    private Integer id_role;

   /* @OneToMany(mappedBy = "s_users_role",cascade = CascadeType.ALL)
    private List<s_users> s_users=new ArrayList<>();*/

   //setter
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_user() {
        return id_user;
    }

    public void setId_user(Integer id_user) {
        this.id_user = id_user;
    }

    public Integer getId_role() {
        return id_role;
    }

    public void setId_role(Integer id_role) {
        this.id_role = id_role;
    }
}
