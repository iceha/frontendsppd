package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "table_pegawai")
public class Pegawai implements Serializable {

    private static final long serialVersionUID = 1620793148562063431L;

    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer nip;

    @Column(nullable = false)
    private String nama;





    @ManyToOne
    @JoinColumn(name = "JABATAN")
    private Jabatan jabatan;

    /*@Column(name = "id_jabatan", nullable = false)
    @JsonProperty("id_jabatan")
    private Integer idJabatan;*/

    //settergetter

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getNip() {
        return nip;
    }

    public void setNip(Integer nip) {
        this.nip = nip;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    /*public Integer getIdJabatan() {
        return idJabatan;
    }

    public void setIdJabatan(Integer idJabatan) {
        this.idJabatan = idJabatan;
    }*/

    public Jabatan getJabatan() {
        return jabatan;
    }

    public void setJabatan(Jabatan jabatan) {
        this.jabatan = jabatan;
    }

}
