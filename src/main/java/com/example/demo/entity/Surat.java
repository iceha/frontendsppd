package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "table_surat")
public class Surat {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_surat", nullable = false)
    @JsonProperty("id_surat")
    private Integer idSurat;

    @Column(name = "no_surat")
    @JsonProperty("no_surat")
    private String noSurat;


    @Column(name = "tanggal_surat")
    @JsonProperty("tanggal_surat")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private Date tanggalSurat;

    @Column(name = "perihal")
    @JsonProperty("perihal")
    private String perihal;

    @Column(name = "kepada")
    @JsonProperty("kepada")
    private String kepada;

    private int no;

    //setter


    public Integer getIdSurat() {
        return idSurat;
    }

    public void setIdSurat(Integer idSurat) {
        this.idSurat = idSurat;
    }

    public String getNoSurat() {
        return noSurat;
    }

    public void setNoSurat(String noSurat) {
        this.noSurat = noSurat;
    }

    public Date getTanggalSurat() {
        return tanggalSurat;
    }

    public void setTanggalSurat(Date tanggalSurat) {
        this.tanggalSurat = tanggalSurat;
    }

    public String getPerihal() {
        return perihal;
    }

    public void setPerihal(String perihal) {
        this.perihal = perihal;
    }

    public String getKepada() {
        return kepada;
    }

    public void setKepada(String kepada) {
        this.kepada = kepada;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }
}
